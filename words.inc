; Я не очень понимаю зачем значение записывается в память не в макросе,
; поэтому я сделал запись в макросе :)

%include "colon.inc"

colon "abc", "abcabcabcabcabc", a

colon "ssssssssss", "ssssssssssssssssssss", b

colon "  a b", "ac", d
