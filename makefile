EXPROG=main
NASM=nasm
NASMFLAGS=-f elf64 -g
LD=ld

.PHONY: build clean rebuild

main.asm: words.inc colon.inc dict.inc lib.inc

dict.asm: lib.inc dict.inc

lib.asm: lib.inc

%.o: %.asm
	$(NASM) $(NASMFLAGS) -o $@ $<


build: ./main.o ./lib.o ./dict.o
	$(LD) -o ./$(EXPROG) ./lib.o ./dict.o ./main.o


clean:
	rm -f ./*.o
	rm ./$(EXPROG)

test:
	python3 test.py

rebuild: clean build
