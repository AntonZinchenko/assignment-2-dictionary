section .text

global exit
global string_length
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


; Принимает код возврата и завершает текущий процесс
exit:
    xor rax, rax
    xor rdi, rdi
    mov rax, 60
    syscall


;DONE
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    xor rcx, rcx
    .loop:
        mov cl, byte[rdi + rax]
        inc rax
        test rcx, rcx
        jnz .loop
    dec rax
    ret
;DONE


;DONE
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdi, 1
    mov rdx, rax
    mov rax, 1
    syscall
    ret
;DONE


;DONE
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA
;DONE


;DONE
; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret
;DONE


;DONE
; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    test rdi, rdi
    jns .not_sign
    push rdi
    mov rdi, 0x2D
    call print_char
    pop rdi
    neg rdi
    .not_sign:
;DONE


;DONE
; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    push r12
    xor r12, r12
    mov r12, rsp
    mov rax, rdi
    mov rcx, 0xA
    sub rsp, 32
    mov byte[rsp], 0x0
    .loop:
        dec rsp
        xor rdx, rdx
        div rcx
        add rdx, 0x30
        mov byte[rsp], dl
        test rax, rax
        jnz .loop
    mov rdi, rsp
    call print_string
    mov rsp, r12
    pop r12
    ret
;DONE


;DONE
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r8, r8
    xor r9, r9
    .loop:
        mov al, byte[rdi+r8]
        mov r9b, byte[rsi+r8]
        cmp rax, r9
        jnz .exit
        inc r8
        test rax, rax
        jnz .loop
        mov rax, 1
        ret
    .exit:
        xor rax, rax
        ret
;DONE


;DONE
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    mov r8, rsp
    sub rsp, 16
    mov byte[rsp], 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    mov rsp, r8
    cmp rax, -1
    jz .error
    mov al, byte[rsi]
    jmp .exit
    .error:
        xor rax, rax
    .exit:
        ret
;DONE


;DONE
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    xor rdx, rdx
    mov r12, 0x1
    xor r13, r13
    xor r14, r14
    mov r14, rdi
    mov r13, rsi
    dec r13
    .start_spaces:
        call read_char
        cmp rax, 0x20
        jz .start_spaces
        cmp rax, 0x9
        jz .start_spaces
        cmp rax, 0xA
        jz .start_spaces
        test rax, rax
        jz .end
        jmp .loop_word
    .loop_word:
        mov byte[r14], al
        inc r14
        inc r12
        call read_char
        cmp rax, 0x20
        jz .end
        cmp rax, 0x9
        jz .end
        cmp rax, 0xA
        jz .end
        cmp r13, r12
        js .error
        test rax, rax
        jz .end
        jmp .loop_word
    .end:
        mov byte[r14], 0x0
        sub r14, r12
        mov rax, r14
        mov rdx, r12
        jmp .exit
    .error:
        mov rax, 0
    .exit:
        pop r14
        pop r13
        pop r12
        ret
;DONE


;DONE
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    push r12
    push r13
    push r14
    xor r12, r12
    xor r13, r13
    mov r14, 0xA
    .loop:
        mov r13b, byte[rdi+r12]
        cmp r13, 0x30 ; сравнение с нулем (0x30 hex код)
        js .exit
        cmp r13, 0x3A ; сравнение с девяткой (0x39 hex код)
        jns .exit
        mul r14
        sub r13, 0x30 ; вычитаю 0x30, поскольку в ascii код цифры вычисляется её значением + 0x30
        add rax, r13
        inc r12
        jmp .loop
    .exit:
        mov rdx, r12
        pop r14
        pop r13
        pop r12
        ret
;DONE


;DONE
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    push r12
    xor r12, r12
    mov r12b, byte[rdi]
    cmp r12, 0x2D
    jz .neg
    call parse_uint
    jmp .exit
    .neg:
        inc rdi
        call parse_uint
        dec rdi
        inc rdx
        neg rax
    .exit:
        pop r12
        ret
;DONE


;DONE
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push r12
    push r13
    xor r13, r13
    xor r12, r12
    .loop:
        mov r13b, byte[rdi]
        mov byte[rsi], r13b
        inc rsi
        inc rdi
        inc r12
        cmp rdx, r12
        js .error
        test r13b, r13b
        jz .exit
        jmp .loop
    .error:
        mov rax, 0
    .exit:
        pop r13
        pop r12
        ret
;DONE

