%include "lib.inc"
%include "dict.inc"
%include "colon.inc"
%include "words.inc"

section .rodata

%define buffer_size 256
error_message_buffer: db "Слишком длинный ключ", 0
error_message_find: db "Ключ не был найден", 0

section .text
global _start
_start:
    push r12
    
    mov r12, rsp
    sub rsp, buffer_size
    mov rdi, rsp
    mov rsi, buffer_size
    call read_key
    test rax, rax
    jz .error_buffer
    
    mov rdi, rsp
    mov rsi, start_el
    call find_word
    test rax, rax
    jz .error_find
    
    mov rdi, rax
    call print_value
    
    mov rsp, r12
    .exit:
        pop r12
        call exit
    .error_buffer:
        mov rdi, error_message_buffer
        call print_string
        jmp .exit
    .error_find:
        mov rdi, error_message_find
        call print_string
        jmp .exit
