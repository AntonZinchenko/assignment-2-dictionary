import subprocess

main_file = "./main"

tests = [["abc", "abcabcabcabcabc"],
         ["ssssssssss", "ssssssssssssssssssss"],
         ["  a b", "ac"],
         ["acsvs", "Ключ не был найден"],
         ["a"*256, "Слишком длинный ключ"],
         ["a"*255, "Ключ не был найден"]]

if __name__ == "__main__":
    print("Тесты выполняются...\n")
    num_test = 1
    incorrect = 0
    count_tests = len(tests)
    for i in tests:
        pop = subprocess.Popen(main_file, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="utf-8")
        stdout, stderr = pop.communicate(i[0]+"\n")

        if stdout != i[1]:
            incorrect += 1
            print(f"Тест {num_test} не пройден")
            print(f"Ключ:{i[0]}")
            print(f"Ожидаемый вывод:{i[1]}")
            print(f"Вывод:{stdout}")
            print("\n")
        num_test += 1

    print("Тесты завершены")
    print(f"Всего тестов: {count_tests}")
    print(f"Пройденных: {count_tests-incorrect}")
    print(f"Не пройденных: {incorrect}")
