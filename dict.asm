%include "lib.inc"
section .text

global find_word
global print_value
global read_key

;rdi - начало буфера
;rsi - размер буфера
read_key:
    push r12
    push r13
    push r14
    xor rdx, rdx
    mov r12, 0x0
    xor r13, r13
    xor r14, r14
    mov r14, rdi
    mov r13, rsi
    dec r13
    .loop_word:
        call read_char
        cmp r13, r12
        js .error
        cmp rax, 0xA
        jz .end
        mov byte[r14], al
        inc r14
        inc r12
        jmp .loop_word
    .end:
        mov byte[r14], 0x0
        sub r14, r12
        mov rax, r14
        mov rdx, r12
        jmp .exit
    .error:
        mov rax, 0
    .exit:
        pop r14
        pop r13
        pop r12
        ret

;rdi - Указатель на нуль-терминированную строку
;rsi - Указатель на начало словаря
find_word:
    ;write your code here
    .next_el_parse:
        add rsi, 8
        push rsi
        call string_equals
        pop rsi
        test rax, rax
        jnz .end
        mov rsi, qword[rsi-8]
        xor rax, rax
        test rsi, rsi
        jz .exit
        jmp .next_el_parse
    .end:
        sub rsi, 8
        mov rax, rsi
    .exit:
        ret

;rdi - Указатель на ноду мапы
print_value:
    add rdi, 8
    call string_length
    add rdi, rax
    inc rdi
    call print_string
    ret