%define start_el 0
%macro colon 3
    %ifnid %3
        %error: "Неверный тип у переданной метки(должна быть метка)"
    %endif
    %ifnstr %2
        %error: "Неверный тип у переданного значения(должна быть строка)"
    %endif
    %ifnstr %1
        %error: "Неверный тип у переданного ключа(должна быть строка)"
    %endif
    %3: dq start_el,
        db %1, 0, %2, 0
    %define start_el %3
%endmacro